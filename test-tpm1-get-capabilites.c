#include <stdio.h>
#include <unistd.h>
#include "tpm.h"

#define TPM_ORD_GET_CAP 101
#define TPM_TAG_RQU_COMMAND 193

enum tpm_capabilities {
	TPM_CAP_FLAG = 4,
	TPM_CAP_PROP = 5,
	TPM_CAP_VERSION_1_1 = 0x06,
	TPM_CAP_VERSION_1_2 = 0x1A,
};

int main()
{
	struct tpm_header *head;
	struct tpm_buf buf;
	int rc, fd;

	fd = open("/dev/tpm0", O_RDWR);
	if (fd < 0) {
		printf("failed to open file\n");
		return fd;
	}
	rc = tpm_buf_init(&buf, TPM_TAG_RQU_COMMAND, TPM_ORD_GET_CAP);
	if (rc) {
		printf("failed to init buf\n");
		return rc;
	}

	tpm_buf_append_u32(&buf, TPM_CAP_VERSION_1_2);
	tpm_buf_append_u32(&buf, 0);
	head = (struct tpm_header *)buf.data;
	rc = write(fd, buf.data, head->length);
	printf("wrote %d bytes\n", rc);
	tpm_buf_destroy(&buf);
	return rc;
}
